let margin = 50;
let mousePosX = 0;
let mousePosY = 0;
let realPosX;
let realPosY;
let sizeX = 100;
let sizeY = 100;
let data;
let command;
let outputCommand;

const API_HOST = "****";

function setup() {
  createCanvas(windowWidth - margin, windowHeight - margin);
  background(200);
  textSize(48);
  mousePosX = margin;
  mousePosY = windowHeight - (margin * 3);
  drawInput();

  data = {
		in: {
			distance: 0,
			interval: setInterval(getData, 100)
		}, 
		out: {
      outputCommand,
			pwm: 0,
			interval: setInterval(postData, 100)
		}
	}

  command = {G: "G00", X: 0, Y: 0,Z: 0, F: 0};
}

function draw() {
  background(200);
  mousePos();
  drawBody();
  drawOutput();
  inputG.value();
  inputG.input(setValueG);
  inputX.value();
  inputX.input(setValueX);
  inputY.value();
  inputY.input(setValueY);
  inputZ.value();
  inputZ.input(setValueZ);
  inputF.value();
  inputF.input(setValueF);
  outputCommand = (command.G + " X:" + command.X + " Y:" + command.Y + " Z:" + command.Z + " F:" + command.F);
}

function mousePos() {
    if (mouseIsPressed === true && mouseButton === LEFT && mouseX <= windowWidth / 2 - margin) {
        mousePosX = constrain(mouseX, margin, windowWidth / 2 - margin * 3);
        mousePosY = constrain(mouseY - margin, margin, windowHeight - (margin * 3));
    }
    realPosX = map(mousePosX, windowWidth / 2 - (margin * 3), margin, sizeX, 0);
    realPosY = map(mousePosY,windowHeight - (margin * 3), margin, 0, sizeY);
}

function drawBody() {
  data.out.pwm = "123";
  data.out.outputCommand = outputCommand;
    rect(margin, margin, windowWidth / 2 - (margin * 3), windowHeight - (margin * 3));
    rect(margin, mousePosY, windowWidth / 2 - (margin * 3), margin);
    rect(mousePosX, margin, margin, windowHeight - (margin * 3));

}

function drawOutput() {
    textSize(48);
    text("G:", windowWidth / 2, margin * 2);
    text("X:", windowWidth / 2, margin * 3);
    text("Y:", windowWidth / 2, margin * 4);
    text("Z:", windowWidth / 2, margin * 5);
    text("F:", windowWidth / 2, margin * 6);
    text("Command:" + command.G + " X:" + command.X + " Y:" + command.Y + " Z:" + command.Z + " F:" + command.F, windowWidth / 2, margin * 7);
}

function drawInput() {
  inputG = createInput("G00");
  inputG.style("font-size", "48px");
  inputG.position((windowWidth / 2) + margin * 1.5 , margin * 1.33);
  inputG.size(100, 48);

  inputX = createInput("0");
  inputX.style("font-size", "48px");
  inputX.position((windowWidth / 2) + margin * 1.5 , margin * 2.33);
  inputX.size(100, 48);

  inputY = createInput("0");
  inputY.style("font-size", "48px");
  inputY.position((windowWidth / 2) + margin * 1.5 , margin * 3.33);
  inputY.size(100, 48);

  inputZ = createInput("0");
  inputZ.style("font-size", "48px");
  inputZ.position((windowWidth / 2) + margin * 1.5 , margin * 4.33);
  inputZ.size(100, 48);

  inputF = createInput("0");
  inputF.style("font-size", "48px");
  inputF.position((windowWidth / 2) + margin * 1.5 , margin * 5.33);
  inputF.size(100, 48);
}

function setValueG() {
    command.G = inputG.value();
}

function setValueX() {
  command.X = inputX.value();
}

function setValueY() {
  command.Y = inputY.value();
}

function setValueZ() {
  command.Z = inputZ.value();
}

function setValueF() {
  command.F = inputF.value();
}

async function getData() {

  console.log("GET request");

  const response = await fetch(API_HOST + "/distance", {
    method: "GET"
  });
  const responseData = await response.json();
  console.log('GET Response:', responseData);

  data.in.distance = responseData.distance;
}

async function postData() {

  console.log("POST request");
  const postData = {
     pwm: data.out.pwm,
     outputCommand: data.out.outputCommand
  };

  const response = await fetch(API_HOST + "/update", {
    method: "POST",
    body: JSON.stringify({
      pwm: data.out.pwm,
      outputCommand: data.out.outputCommand
    })
  });
  const responseData = await response.json();
  console.log(responseData);
}