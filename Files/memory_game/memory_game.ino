#include <Adafruit_NeoPixel.h>
 
int powPin = NEOPIXEL_POWER;
int neoPin = PIN_NEOPIXEL;
int btnPin = D1;

#define NUM_PIXELS 1
 
Adafruit_NeoPixel pixels(NUM_PIXELS, neoPin, NEO_GRB + NEO_KHZ800);

String command;
bool running = false;
int counter = 0;
String currentcolor = "";
String seq = "PBBWWPWP";
String newSeq ="";
int seqLength = 5;

unsigned long prevMillis = 0;

int buttonValue = HIGH;

int prevButtonValue = HIGH;

bool timerOn = false;

unsigned long startMillis = 0;

int timer = 250;

int clicks = 0;

unsigned long clickMillis = 0;

String inputSeq;

int inputCounter = 0;

String input;

void setup() {
  Serial.begin(9600);
  Serial.setTimeout(100);
  pixels.begin();
  pinMode(powPin, OUTPUT);
  pinMode(btnPin, INPUT_PULLUP);
  digitalWrite(powPin, LOW);
  digitalWrite(powPin, HIGH);
  pixels.setBrightness(25);
  Serial.println(seq);

}

void loop() {
  if (running == true) {
    if (counter < newSeq.length()) {
      currentcolor = newSeq.substring(counter, counter + 1);
      Serial.println(currentcolor);
      counter = counter +1;
      if (currentcolor == "0") {
        digitalWrite(powPin, HIGH);
        delay(10);
        pixels.clear();
        pixels.setPixelColor(0, pixels.Color(255, 55, 155));
        pixels.show();
        delay(500);
        digitalWrite(powPin, LOW);
        delay(1000);
      }

      else if (currentcolor == "1") {
        digitalWrite(powPin, HIGH);
        delay(10);
        pixels.clear();
        pixels.setPixelColor(0, pixels.Color(0, 0, 255));
        pixels.show();
        delay(250);
        digitalWrite(powPin, LOW);
        delay(250);
        digitalWrite(powPin, HIGH);
        delay(10);
        pixels.clear();
        pixels.setPixelColor(0, pixels.Color(0, 0, 255));
        pixels.show();
        delay(250);
        digitalWrite(powPin, LOW);
        delay(1000);
      }

      else if (currentcolor == "2") {
        digitalWrite(powPin, HIGH);
        delay(10);
        pixels.clear();
        pixels.setPixelColor(0, pixels.Color(255, 255, 255));
        pixels.show();
        delay(1500);
        digitalWrite(powPin, LOW);
        delay(1000);
      }
    }

    else {
      running = false;
      counter = 0;
      digitalWrite(powPin, LOW);
    }

  }

  buttonValue = digitalRead(D1);
  
  if (buttonValue == LOW && prevButtonValue == HIGH) {
    clicks += 1;
    timerOn = true;
    startMillis = millis();
    clickMillis = millis();
  }

  else if (buttonValue == HIGH && prevButtonValue == LOW) {
    if ((millis() - clickMillis) > 200){
      input += "long";
    }
    else {
      input += "short";
    }
  }

  else if (timerOn == true) {
    if ((startMillis + timer < millis()) && (buttonValue == HIGH) ) {
      timerOn = false;
      startMillis = 0;
      if (input == "short") {
        inputSeq += "0";
        //Serial.println("short");
      }
      else if (input == "shortshort") {
        inputSeq += "1";
        //Serial.println("double");
      }
      else if (input == "long") {
        inputSeq += "2";
        //Serial.println("long");
      }
      else {
        inputSeq += "X";
        Serial.println("unknown command");
      }
      inputCounter += 1;
      input = "";
      clicks = 0;
      //Serial.println(newSeq.substring(inputCounter - 1, inputCounter));

      if (newSeq.substring(inputCounter - 1, inputCounter) == inputSeq.substring(inputCounter - 1, inputCounter)) {
        Serial.println("correct");
      }

      else {
        Serial.println("incorrect");
      }
    }

  }
  prevButtonValue = buttonValue;
  delay(10);

  if (Serial.available()) {
    command = Serial.readStringUntil('\n');

    if (command.equals("mode1")) {
      Serial.println("mode 1 selected");
      digitalWrite(powPin, HIGH);
      delay(10);
      pixels.clear();
      pixels.setPixelColor(0, pixels.Color(255, 255, 255));
      pixels.show();
    }

    else if (command.equals("mode2")) {
      Serial.println("mode 2 selected");
      digitalWrite(powPin, LOW);
    }

    else if (command.equals("remove")) {
      seq.remove(0, 1);
      Serial.println(seq);
    }

    else if (command.equals("part")) {
      Serial.println(seq.substring(0,1));
    }

    else if (command.equals("run")) {
      running = true;
    }

    else if (command.equals("mode3")) {
      Serial.println("mode 3 selected");
      delay(10);
      digitalWrite(powPin, HIGH);
      pixels.clear();
      pixels.setPixelColor(0, pixels.Color(0, 255, 255));
      pixels.show();
    }

    else if (command == "new seq") {
      newSeq = "";
      inputSeq = "";
      inputCounter = 0;
      Serial.println("new sequence");
      for (int i = 0; i <=seqLength; i++) {
        newSeq += random(0,3);
      }
      Serial.println(newSeq);
    }


    else {
      Serial.println("unknown command");
    }
  }
}
